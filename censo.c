/*********************************************
 *	Vitor Nere Araújo Ribeiro - 13/0137413   *
 *	Leonardo Sagmeister de Melo - 13/0120332 *
 *											 *
 *********************************************/
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef struct Cidade {
	char *nome;
	char *estado;
	int sRendimento;
	int umSalarioM;
	int udSalarioM; /* 1 a 2 salários mínimos */
	int dtSalarioM; /* 2 a 3 salarios mínimos */
	int tcSalarioM; /* 3 a 5 salarios mínimos */
	int cdSalarioM; /* 5 a 10 salarios mínimos */
	int dvSalarioM; /* 10 a 20 salarios mínimos */
	int maisVsalarioM; /* 20 mais salarios mínimos */
	int totalPessoas;
	float percentSemRendimento; /* percentual sem rendimento */
	float percentAteDois; /* percentual até dois salários mínimos */
	float percentMaisVinte; /* percentual mais de vinte salários mínimos */
} cidade;

typedef struct Lista {
	struct Cidade *cidade;
	struct Lista *prox;
} lista;

typedef struct Cabecalho {
	int nElem;
	struct Lista *inilist;
	struct Lista *fimlist;
} cabecalho;

void cadastro(cabecalho *cab);
void consulta(cabecalho *cab);
int excluir(cabecalho *cab);
void exibir(cabecalho *cab);
void pMenu();
void ler_arquivo(cabecalho *cab);
void adicionar_arq(cabecalho *cab);
void gerar_relatorio(cabecalho *cab);

int main() {
	char menu;

	cabecalho *cab = (cabecalho*) malloc(sizeof(cabecalho));
	cab -> inilist = NULL;
	cab -> fimlist = NULL;
	cab -> nElem = 0;
	ler_arquivo(cab);
	do {
		do {
			menu = '0';
			pMenu();
			scanf(" %c", &menu);
			if(menu > '6' || menu < '1')
				printf("Opcao invalida!\n");
		} while (menu > '6' || menu < '1');
		switch(menu) {
			case '1':
				cadastro(cab);
				break;
				
			case '2':
				consulta(cab);
				break;
				
			case '3':
				if(excluir(cab))
					printf("\nCidade excluída com suceso.\n");
				break;
				
			case '4':
				exibir(cab);
				break;
				
			case '5':
				gerar_relatorio(cab);
				break;
				
			case '6':
				adicionar_arq(cab);
				break;
			
			default:
				break;
		}
	
	
	} while(menu != '6');

return 0;
}

void toUP ( char *p ) { 
	while( *p ) { 
		*p=toupper( *p ); 
		p++; 
	}
}

int ordena(cabecalho *cab, cidade *novaCidade) {
	lista *aux = (lista*) malloc(sizeof(lista));
	lista *aux2;
	lista *auxant = NULL;
	int i;
	char *nome1, *nome2; /* Guarda as cidades em maiuculo para comparação */
	aux -> cidade = novaCidade;
	aux2 = cab -> inilist;

	
	nome1 = (char*) malloc(strlen(aux->cidade->nome)*sizeof(char));
	strcpy(nome1, aux->cidade->nome);
	toUP(nome1);
	
	if(cab->nElem == 0) {
			cab -> inilist = cab -> fimlist = aux;
			aux -> prox = NULL;
			cab -> nElem ++;
			return 1;
	}
	
	for(i=0; i<cab->nElem; i++) {
		if(strcmp(aux->cidade->nome, aux2->cidade->nome) == 0) {
			return 0;
		}
		else
			aux2 = aux2 -> prox;
	}
	
	aux2 = cab -> inilist;
	nome2 = (char*) malloc(strlen(aux2->cidade->nome)*sizeof(char));
	strcpy(nome2, aux2->cidade->nome);
	toUP(nome2);
	
	for(i=0; i<cab->nElem; i++) {				
		if(strcmp(nome1, nome2) < 0) {
			if(auxant == NULL) {
				aux -> prox = cab -> inilist;
				cab -> inilist = aux;
				cab -> nElem ++;
				return 1;
			}
			else {
				aux -> prox = aux2;
				auxant -> prox = aux;
				cab -> nElem ++;
				return 1;
			}
			
		}
		else if(aux2 -> prox == NULL){
			aux -> prox = aux2 -> prox;
			aux2 -> prox = aux;
			cab -> fimlist = aux;
			cab -> nElem ++;
			return 1;
		}
		auxant = aux2;
		aux2 = aux2 -> prox;
		free(nome2);
		nome2 = (char*) malloc(strlen(aux2->cidade->nome)*sizeof(char));
		strcpy(nome2, aux2->cidade->nome);
		toUP(nome2);
	}
	printf("\nOcorreu algum erro na ordenação.\n");
	return 0;
}

void erroTotalPessoas(int totalPessoas, int correcao) {
	printf("Erro, foi declarado que o total de pessoas são %d, mas já foi ultrapassado o total.\n", totalPessoas);
	printf("Foi atribuido a pergunta anterior %d quantidade. Lembrando que a partir de agora\n", correcao);
	printf("todos os demais serão automáticamente atribuido ZERO, devido ao estouro do total de pessoas.\n");
}

void cadastro(cabecalho *cab) {
	cidade *novaCidade = (cidade*) malloc(sizeof(cidade));
	char nome[50];
	char estado[2];
	int contMAX = 0, correcao;
	novaCidade->totalPessoas = 0;
		
	printf("Cidade: ");
	scanf("%s", nome);
	novaCidade -> nome = malloc(strlen(nome)*sizeof(char));
	strcpy(novaCidade->nome, nome);
	
	printf("\nEstado: ");
	scanf("%s", estado);
	toUP(estado);
	novaCidade -> estado = malloc(2*sizeof(char));
	strcpy(novaCidade->estado, estado);
	
	printf("Número total de pessoas: ");
	scanf("%d", &novaCidade -> totalPessoas);
	
	printf("\nNúmero de pessoas sem rendimento: ");
	scanf("%d", &novaCidade -> sRendimento);
	contMAX += novaCidade -> sRendimento;
	if(contMAX > novaCidade->totalPessoas) {
		correcao = contMAX - novaCidade->totalPessoas;
		erroTotalPessoas(novaCidade->totalPessoas, correcao);
		novaCidade->sRendimento = correcao;
	}
	
	printf("\nNúmero de pessoas com até 1 salário mínimo: ");
	scanf("%d", &novaCidade -> umSalarioM);
	contMAX += novaCidade -> umSalarioM;
	if(correcao > 0)
		novaCidade -> umSalarioM = 0;
	else if(contMAX > novaCidade->totalPessoas) {
		correcao = contMAX - novaCidade->totalPessoas;
		erroTotalPessoas(novaCidade->totalPessoas, correcao);
		novaCidade->umSalarioM = correcao;
	}
	
	printf("\nNúmero de pessoas com 1 a 2 salários mínimos: ");
	scanf("%d", &novaCidade -> udSalarioM);
	contMAX += novaCidade -> udSalarioM;
	if(correcao > 0)
		novaCidade -> udSalarioM = 0;
	else if(contMAX > novaCidade->totalPessoas) {
		correcao = contMAX - novaCidade->totalPessoas;
		erroTotalPessoas(novaCidade->totalPessoas, correcao);
		novaCidade->udSalarioM = correcao;
	}
	
	printf("\nNúmero de pessoas com 2 a 3 salários mínimos: ");
	scanf("%d", &novaCidade -> dtSalarioM);
	contMAX += novaCidade -> dtSalarioM;
	if(correcao > 0)
		novaCidade -> dtSalarioM = 0;
	else if(contMAX > novaCidade->totalPessoas) {
		correcao = contMAX - novaCidade->totalPessoas;
		erroTotalPessoas(novaCidade->totalPessoas, correcao);
		novaCidade->dtSalarioM = correcao;
	}
	
	printf("\nNúmero de pessoas com 3 a 5 salários mínimos: ");
	scanf("%d", &novaCidade -> tcSalarioM);
	contMAX += novaCidade -> tcSalarioM;
	if(correcao > 0)
		novaCidade -> tcSalarioM = 0;
	else if(contMAX > novaCidade->totalPessoas) {
		correcao = contMAX - novaCidade->totalPessoas;
		erroTotalPessoas(novaCidade->totalPessoas, correcao);
		novaCidade->tcSalarioM = correcao;
	}
	
	printf("\nNúmero de pessoas com 5 a 10 salários mínimos: ");
	scanf("%d", &novaCidade -> cdSalarioM);
	contMAX += novaCidade -> cdSalarioM;
	if(correcao > 0)
		novaCidade -> cdSalarioM = 0;
	else if(contMAX > novaCidade->totalPessoas) {
		correcao = contMAX - novaCidade->totalPessoas;
		erroTotalPessoas(novaCidade->totalPessoas, correcao);
		novaCidade->cdSalarioM = correcao;
	}
	
	printf("\nNúmero de pessoas com 10 a 20 salários mínimos: ");
	scanf("%d", &novaCidade -> dvSalarioM);
	contMAX += novaCidade -> dvSalarioM;
	if(correcao > 0)
		novaCidade -> dvSalarioM = 0;
	else if(contMAX > novaCidade->totalPessoas) {
		correcao = contMAX - novaCidade->totalPessoas;
		erroTotalPessoas(novaCidade->totalPessoas, correcao);
		novaCidade->dvSalarioM = correcao;
	}
	
	printf("\nNúmero de pessoas com mais de 20 salários mínimos: ");
	scanf("%d", &novaCidade -> maisVsalarioM);
	contMAX += novaCidade -> maisVsalarioM;
	if(correcao > 0)
		novaCidade -> maisVsalarioM = 0;
	else if(contMAX > novaCidade->totalPessoas) {
		correcao = contMAX - novaCidade->totalPessoas;
		erroTotalPessoas(novaCidade->totalPessoas, correcao);
		novaCidade->maisVsalarioM = correcao;
	}
	
	novaCidade->percentSemRendimento = 100 * novaCidade->sRendimento / novaCidade->totalPessoas;
	novaCidade->percentAteDois = 100 * (novaCidade->umSalarioM + novaCidade->udSalarioM) / novaCidade->totalPessoas;
	novaCidade->percentMaisVinte = 100 * novaCidade->maisVsalarioM / novaCidade-> totalPessoas;	
	
	if(ordena(cab, novaCidade)) {
		printf("\nCidade cadastrada com sucesso.\n");
	}
	else
		printf("\nCidade já existente na lista.\n");
}

void consulta(cabecalho *cab) {
	char cidade[50];
	int i;
	float umSalarioM, dtSalarioM, tcSalarioM, cdSalarioM, dvSalarioM;
	lista *aux = cab -> inilist;
	
	if(cab->nElem == 0) {
		printf("\nLista de cidades encontra-se vazia.\n");
		return;
	}
	
	printf("Qual cidade deseja consultar: ");
	scanf("%s", cidade);
	
	for(i=0; i<cab->nElem; i++) {
		if(strcmp(cidade, aux->cidade->nome) == 0) {
			umSalarioM = (float) 100*aux->cidade->umSalarioM/aux->cidade->totalPessoas;
			dtSalarioM = (float) 100*aux->cidade->dtSalarioM/aux->cidade->totalPessoas;
			tcSalarioM = (float) 100*aux->cidade->tcSalarioM/aux->cidade->totalPessoas;
			cdSalarioM = (float) 100*aux->cidade->cdSalarioM/aux->cidade->totalPessoas;
			dvSalarioM = (float) 100*aux->cidade->dvSalarioM/aux->cidade->totalPessoas;
			printf("\nCidade: %s.\n", aux->cidade->nome);
			printf("Estado: %s.                                            Percentual:\n", aux->cidade->estado);
			printf("Número de pessoas sem rendimento: %d                      %.2f%\n", aux->cidade->sRendimento, aux->cidade->percentSemRendimento);
			printf("Número de pessoas com até 1 salário mínimo: %d            %.2f%\n", aux->cidade->umSalarioM, umSalarioM);
			printf("Número de pessoas com 1 a 2 salários mínimos: %d          %.2f%\n", aux->cidade->udSalarioM, dtSalarioM);
			printf("Número de pessoas com 2 a 3 salários mínimos: %d          %.2f%\n", aux->cidade->dtSalarioM, dtSalarioM);
			printf("Número de pessoas com 3 a 5 salários mínimos: %d          %.2f%\n", aux->cidade->tcSalarioM, tcSalarioM);
			printf("Número de pessoas com 5 a 10 salários mínimos: %d         %.2f%\n", aux->cidade->cdSalarioM, cdSalarioM);
			printf("Número de pessoas com 10 a 20 salários mínimos: %d        %.2f%\n", aux->cidade->dvSalarioM, dvSalarioM);
			printf("Número de pessoas com mais de 20 salarios mínimos: %d     %.2f%\n", aux->cidade->maisVsalarioM, aux->cidade->percentMaisVinte);
			printf("Total de pessoas: %d\n", aux->cidade->totalPessoas);
			return;
		}
		aux = aux -> prox;
	}
	
	printf("\nCidade não encontrada.\n");
}

int excluir(cabecalho *cab) {
	char cidade[50];
	int i;
	lista *aux = cab->inilist;
	lista *auxant = NULL;
	
	if(cab->nElem == 0) {
		printf("\nLista de cidades encontra-se vazia.\n");
		return 0;
	}
	
	printf("Digite a cidade que deseja excluir: ");
	scanf("%s", cidade);
	
	for(i=0; i<cab->nElem; i++) {
		if(strcmp(cidade, aux->cidade->nome) == 0) {
			if(cab->nElem == 1) {
				cab -> inilist = cab -> fimlist = NULL;
				cab -> nElem = 0;
				free(aux->cidade);
				free(aux);
				return 1;
			}
			else if(auxant == NULL) {
				cab -> inilist = aux -> prox;
				cab -> nElem --;
				free(aux->cidade);
				free(aux);
				return 1;
			}
			else{
				auxant -> prox = aux -> prox;
				if(aux -> prox == NULL)
					cab -> fimlist = auxant;
				cab -> nElem --;
				free(aux->cidade);
				free(aux);
				return 1;
			}			
		}
		auxant = aux;
		aux = aux -> prox;
	}
	
	printf("\nCidade não encontrada.\n");
	return 0;
}

void exibir(cabecalho *cab) {
	int i;
	lista *aux = cab -> inilist;
	
	if(cab->nElem == 0) {
		printf("\nLista de cidades encontra-se vazia.\n");
		return;
	}
	
	printf("\n");
	
	for(i=0; i<cab->nElem; i++) {
		printf("%s, ", aux->cidade->nome);
		aux = aux -> prox;
	}
	printf(".\n");
}

void pMenu() {
	printf("\n (1) Cadastrar os dados de rendimento de uma cidade.");
	printf("\n (2) Consultar os dados de uma cidade.");
	printf("\n (3) Excluir uma cidade.");
	printf("\n (4) Mostrar a relação de cidades.");
	printf("\n (5) Gerar relatório de cidades.");
	printf("\n (6) Sair do programa.");
	printf("\n Opção: ");
}

void ler_arquivo(cabecalho *cab) {
	FILE *arquivo;
	cidade *nova_cidade;
	char nome[50], estado[2];
	int sRendimento, umSalarioM, udSalarioM, dtSalarioM, tcSalarioM, cdSalarioM, dvSalarioM, maisVsalarioM, totalPessoas;
	
	arquivo = fopen("censo.txt", "r+");
	if (arquivo == NULL)
		return;
	while(!feof(arquivo)) {

		nova_cidade = (cidade*)malloc(sizeof(cidade));
		if(fscanf(arquivo,"%s",nome) == EOF)
			return;
		fscanf(arquivo,"%s", estado);
		 
		fscanf(arquivo, "%d\n%d\n%d\n%d\n%d\n%d\n%d\n%d\n%d\n", &sRendimento, &umSalarioM, &udSalarioM, &dtSalarioM, &tcSalarioM, &cdSalarioM, &dvSalarioM, &maisVsalarioM, &totalPessoas);
		
		nova_cidade = malloc(sizeof(cidade));
		nova_cidade->nome = malloc(strlen(nome)*sizeof(char));
		strcpy(nova_cidade->nome, nome);
		nova_cidade->estado = malloc(strlen(estado)*sizeof(char));
		strcpy(nova_cidade->estado, estado);
		nova_cidade->sRendimento = sRendimento;
		nova_cidade->umSalarioM = umSalarioM;
		nova_cidade->udSalarioM = udSalarioM;
		nova_cidade->dtSalarioM = dtSalarioM;
		nova_cidade->tcSalarioM = tcSalarioM;
		nova_cidade->cdSalarioM = cdSalarioM;
		nova_cidade->dvSalarioM = dvSalarioM;
		nova_cidade->maisVsalarioM = maisVsalarioM;
		nova_cidade->totalPessoas = totalPessoas;
		nova_cidade->percentSemRendimento = 100 * nova_cidade->sRendimento / nova_cidade->totalPessoas;
		nova_cidade->percentAteDois = 100 * (nova_cidade->umSalarioM + nova_cidade->udSalarioM) / nova_cidade->totalPessoas;
		nova_cidade->percentMaisVinte = 100 * nova_cidade->maisVsalarioM / nova_cidade-> totalPessoas;
		
		if(!ordena(cab, nova_cidade))
			printf("\nErro ocorrido ou duas cidades iguais na lista.\n");
	}	
	fclose(arquivo);
}

void adicionar_arq(cabecalho *cab) {
	FILE *arquivo;
	lista *aux;
	int i;
	
	if (cab->nElem == 0) 
		return;
		
	arquivo = fopen("censo.txt", "w");
	aux = cab->inilist;
	
	for (i = 0; i < cab->nElem; i++) {
		fprintf(arquivo, "%s\n", aux->cidade->nome);
		fprintf(arquivo, "%s\n", aux->cidade->estado);
		fprintf(arquivo, "%d\n", aux->cidade->sRendimento);
		fprintf(arquivo, "%d\n", aux->cidade->umSalarioM);
		fprintf(arquivo, "%d\n", aux->cidade->udSalarioM);
		fprintf(arquivo, "%d\n", aux->cidade->dtSalarioM);
		fprintf(arquivo, "%d\n", aux->cidade->tcSalarioM);
		fprintf(arquivo, "%d\n", aux->cidade->cdSalarioM);
		fprintf(arquivo, "%d\n", aux->cidade->dvSalarioM);
		fprintf(arquivo, "%d\n", aux->cidade->maisVsalarioM);
		fprintf(arquivo, "%d\n", aux->cidade->totalPessoas);
		aux = aux -> prox;
	}
	
	fclose(arquivo);
	
	return;
}

void gerar_relatorio(cabecalho *cab) {
	FILE *relatorio;
	lista *aux = cab->inilist;
	float auxiliar;
	float percent_cidades[cab->nElem];
	int i, j;
	
	if(cab->nElem == 0) {
		printf("\nLista de cidades encontra-se vazia.\n");
		return;
	}
	
	relatorio = fopen("relatorio.txt", "w");
	
	for (i = 0; i < cab->nElem; i++) {
		percent_cidades[i] = aux->cidade->percentSemRendimento;
		aux = aux->prox;
	}
	
	for (i = 0; i < cab->nElem; i++) {
		for (j = 0; j < cab->nElem; j++) {
			if (percent_cidades[i] > percent_cidades[j]) {
				auxiliar = percent_cidades[i];
				percent_cidades[i] = percent_cidades[j];
				percent_cidades[j] = auxiliar;
			}
		}
	}
	
	fprintf(relatorio, "-----------------------------------------------------------------\n");
	fprintf(relatorio, "Cidades por ordem crescente de percentual de pessoas sem rendimento:\n");
	i = 0;
	while (i < cab->nElem) {
		for (aux = cab->inilist; aux != NULL; aux = aux->prox) {
			if (aux->cidade->percentSemRendimento == percent_cidades[i]) {
				fprintf(relatorio, "%s      %.2f%\n", aux->cidade->nome, percent_cidades[i]);
				i++;
			}
		}
	}
	
	aux = cab->inilist;
	
	for (i = 0; i < cab->nElem; i++) {
		percent_cidades[i] = aux->cidade->percentAteDois;
		aux = aux->prox;
	}
	
	for (i = 0; i < cab->nElem; i++) {
		for (j = 0; j < cab->nElem; j++) {
			if (percent_cidades[i] > percent_cidades[j]) {
				auxiliar = percent_cidades[i];
				percent_cidades[i] = percent_cidades[j];
				percent_cidades[j] = auxiliar;
			}
		}
	}
	
	fprintf(relatorio, "-----------------------------------------------------------------\n");
	fprintf(relatorio, "-----------------------------------------------------------------\n");
	fprintf(relatorio, "Cidades por ordem crescente de percentual de pessoas até dois salários mínimos:\n");
	i = 0;
	while (i < cab->nElem) {
		for (aux = cab->inilist; aux != NULL; aux = aux->prox) {
			if (aux->cidade->percentAteDois == percent_cidades[i]) {
				fprintf(relatorio, "%s      %.2f%\n", aux->cidade->nome, percent_cidades[i]);
				i++;
			}
		}
	}
	
	aux = cab->inilist;
	
	for (i = 0; i < cab->nElem; i++) {
		percent_cidades[i] = aux->cidade->percentMaisVinte;
		aux = aux->prox;
	}
	
	for (i = 0; i < cab->nElem; i++) {
		for (j = 0; j < cab->nElem; j++) {
			if (percent_cidades[i] > percent_cidades[j]) {
				auxiliar = percent_cidades[i];
				percent_cidades[i] = percent_cidades[j];
				percent_cidades[j] = auxiliar;
			}
		}
	}
	
	fprintf(relatorio, "-----------------------------------------------------------------\n");
	fprintf(relatorio, "-----------------------------------------------------------------\n");
	fprintf(relatorio, "Cidades por ordem crescente de percentual de pessoas acima de 20 salarios mínimos:\n");
	i = 0;
	while (i < cab->nElem) {
		for (aux = cab->inilist; aux != NULL; aux = aux->prox) {
			if (aux->cidade->percentMaisVinte == percent_cidades[i]) {
				fprintf(relatorio, "%s      %.2f%\n", aux->cidade->nome, percent_cidades[i]);
				i++;
			}
		}
	}
	fprintf(relatorio, "-----------------------------------------------------------------\n");
	fclose(relatorio);
	
	printf("\nRelatório gerado.\n");
}			
